# About the Project
LmcdoCom is the official website of Luke McDougald.

## How is this related to Loafing.Dev?
At this time I am the primary developer working on Loafing.Dev projects but that could change. This site is separate to make sure that Loafing.Dev can accomodate other developers going forward.

## Will any projects be posted on this site?
No, only on Loafing.Dev. This site is purely a showcase portfolio.

## Can I use code from this site?
Feel free to use small snippets but please ask before using large portions of the theme.
